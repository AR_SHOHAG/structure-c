#include<stdio.h>
#include<string.h>
#define SIZE 3

struct diu_cse
{
    char name[20];
    int id;
    float cgpa;
};

struct diu_cse student[SIZE];

void input(struct diu_cse student[])
{
    int i;
    printf("***********Input************\n");
    for(i=0;i<SIZE;i++){
        scanf("%s%d%f", student[i].name, &student[i].id, &student[i].cgpa);
    }
}

void sort_name_student(struct diu_cse student[])
{
    int i,j;
    for(i=0;i<SIZE-1;i++){
        for(j=i+1;j<SIZE;j++){
            if(strcmp(student[i].name , student[j].name)>0){
                struct diu_cse temp;

                strcpy(temp.name , student[i].name);
                temp.id = student[i].id;
                temp.cgpa = student[i].cgpa;

                strcpy(student[i].name , student[j].name);
                student[i].id = student[j].id;
                student[i].cgpa = student[j].cgpa;

                strcpy(student[j].name , temp.name);
                student[j].id = temp.id;
                student[j].cgpa = temp.cgpa;
            }
        }
    }
}

void output(struct diu_cse student[])
{
    int i;
    printf("***********Output************\n");
    for(i=0;i<SIZE;i++){
        printf("%s %d %.2f\n", student[i].name, student[i].id, student[i].cgpa);

    }
}


int main()
{
    input(student);
    sort_name_student(student);
    output(student);
}
